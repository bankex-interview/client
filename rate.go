package main

import (
	"gitlab.com/bankex-interview/client/cmd"
)

func main() {
	cmd.Execute()
}

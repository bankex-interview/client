package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/spf13/cobra"
	flag "github.com/spf13/pflag"
)

var Pair string

func init() {
	flag.StringVar(&Pair, "pair", "", "pair to get price")
}

var rootCmd = &cobra.Command{
	Use:   "",
	Short: "Hugo is a very fast static site generator",
	Long: `A Fast and Flexible Static Site Generator built with
                love by spf13 and friends in Go.
                Complete documentation is available at http://hugo.spf13.com`,
	Run: func(cmd *cobra.Command, args []string) {
		req, err := http.NewRequest("GET", fmt.Sprintf("http://localhost:3001/api/v1/rates?pairs=%s", Pair), nil)
		if err != nil {
			log.Println(err)
			return
		}
		client := http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			log.Println(err)
			return
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
			return
		}
		defer resp.Body.Close()
		result := map[string]string{}
		err = json.Unmarshal(body, &result)
		if err != nil {
			log.Println(err)
			return
		}
		keys := strings.Split(Pair, ",")
		for _, key := range keys {
			fmt.Println(key, result[key])
		}
	},
}

func Execute() {
	flag.Parse()
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
